#ifndef FTS_EDGE_H
#define FTS_EDGE_H

#include <vector>

namespace fts
{

	template<class GlobalPropT, class VertexPropT, class EdgePropT>
	class Instance;

	template<class InstPropU, class VertexPropU, class PropT>
	class Edge
	{
	private:
		typedef Instance<InstPropU, VertexPropU, PropT> instance_t;
		typedef typename instance_t::edge_t self_t;
		typedef typename instance_t::vertex_t vertex_t;
		typedef typename instance_t::edge_hand_t self_hand_t;
		typedef typename instance_t::c_edge_hand_t c_self_hand_t;
		typedef typename instance_t::edge_meet_t self_meet_t;
		typedef typename instance_t::c_edge_meet_t c_self_meet_t;
		typedef typename instance_t::self_meet_t instance_meet_t;
		typedef typename instance_t::c_self_meet_t c_instance_meet_t;
		typedef typename instance_t::vertex_meet_t vertex_meet_t;
		typedef typename instance_t::c_vertex_meet_t c_vertex_meet_t;
		PropT properties;
		vertex_meet_t source();
		vertex_meet_t destination();
	public:
		Edge();
	};

}

#endif//FTS_EDGE_H
