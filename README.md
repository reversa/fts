Flavored Transition System
==========================

A part of works from REVERSA project.

Flavored Transition System is designed to extend Labeled Transition System with extra properties, such as process algebraic semantics in CCS/ACSR theories.
