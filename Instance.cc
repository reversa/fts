#include "Instance.h"
#include "Vertex.h"
#include "Edge.h"

namespace fts
{

	template<class GlobalPropT, class VertexPropT, class EdgePropT>
	Instance<GlobalPropT, VertexPropT, EdgePropT>::Instance
	(
	)
	{
	}

	template<class GlobalPropT, class VertexPropT, class EdgePropT>
		typename Instance<GlobalPropT, VertexPropT, EdgePropT>::vertex_meet_t
	Instance<GlobalPropT, VertexPropT, EdgePropT>::create_vertex
	(
		::std::vector<typename Instance<GlobalPropT, VertexPropT, EdgePropT>::edge_meet_t> incomings,
		::std::vector<typename Instance<GlobalPropT, VertexPropT, EdgePropT>::edge_meet_t> outgoings
	)
	{
	}

	template<class GlobalPropT, class VertexPropT, class EdgePropT>
		typename Instance<GlobalPropT, VertexPropT, EdgePropT>::edge_meet_t
	Instance<GlobalPropT, VertexPropT, EdgePropT>::create_edge
	(
		typename Instance<GlobalPropT, VertexPropT, EdgePropT>::vertex_meet_t source,
		typename Instance<GlobalPropT, VertexPropT, EdgePropT>::vertex_meet_t destination
	)
	{
	}

}
