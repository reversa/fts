#ifndef FTS_VERTEX_H
#define FTS_VERTEX_H

#include <vector>

namespace fts
{

	template<class GlobalPropT, class VertexPropT, class EdgePropT>
	class Instance;

	template<class InstPropU, class PropT, class EdgePropU>
	class Vertex
	{
	private:
		typedef Instance<InstPropU, PropT, EdgePropU> instance_t;
		typedef typename instance_t::vertex_t self_t;
		typedef typename instance_t::edge_t edge_t;
		typedef typename instance_t::vertex_hand_t self_hand_t;
		typedef typename instance_t::c_vertex_hand_t c_self_hand_t;
		typedef typename instance_t::vertex_meet_t self_meet_t;
		typedef typename instance_t::c_vertex_meet_t c_self_meet_t;
		typedef typename instance_t::edge_meet_t edge_meet_t;
		typedef typename instance_t::c_edge_meet_t c_edge_meet_t;
		PropT properties;
		::std::vector<edge_meet_t> incomings();
		::std::vector<edge_meet_t> outgoings();
	public:
		Vertex();
	};

}

#endif//FTS_VERTEX_H
