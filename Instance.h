#ifndef FTS_INSTANCE_H
#define FTS_INSTANCE_H

#include <vector>
#include <memory>

namespace fts
{

	template<class InstPropU, class PropT, class EdgePropU>
	class Vertex;
	template<class InstPropU, class VertexPropU, class PropT>
	class Edge;

	template<class GlobalPropT, class VertexPropT, class EdgePropT>
	class Instance
	{
	private:
		template<typename T>
		using own = typename ::std::unique_ptr<T>;
		template<typename T>
		using hand = typename ::std::shared_ptr<T>;
		template<typename T>
		using meet = typename ::std::weak_ptr<T>;
	public:
		typedef Instance<GlobalPropT, VertexPropT, EdgePropT> self_t;
		typedef Vertex<GlobalPropT, VertexPropT, EdgePropT> vertex_t;
		typedef Edge<GlobalPropT, VertexPropT, EdgePropT> edge_t;
		typedef own<vertex_t> vertex_own_t;
		typedef own<const vertex_t> c_vertex_own_t;
		typedef own<edge_t> edge_own_t;
		typedef own<const edge_t> c_edge_own_t;
		typedef hand<self_t> self_hand_t;
		typedef hand<const self_t> c_self_hand_t;
		typedef hand<vertex_t> vertex_hand_t;
		typedef hand<const vertex_t> c_vertex_hand_t;
		typedef hand<edge_t> edge_hand_t;
		typedef hand<const edge_t> c_edge_hand_t;
		typedef meet<self_t> self_meet_t;
		typedef meet<const self_t> c_self_meet_t;
		typedef meet<vertex_t> vertex_meet_t;
		typedef meet<const vertex_t> c_vertex_meet_t;
		typedef meet<edge_t> edge_meet_t;
		typedef meet<const edge_t> c_edge_meet_t;
	private:
		GlobalPropT properties;
		::std::vector<vertex_own_t> owned_vertices;
		::std::vector<edge_own_t> owned_edges;
		::std::vector<self_hand_t> sub_instances;
		::std::vector<vertex_meet_t> all_vertices();
		::std::vector<edge_meet_t> all_edges();
	public:
		Instance();
		vertex_meet_t create_vertex(::std::vector<edge_meet_t> incomings = {}, ::std::vector<edge_meet_t> outgoings = {});
		edge_meet_t create_edge(vertex_meet_t source = nullptr, vertex_meet_t destination = nullptr);
	};

}

#endif//FTS_INSTANCE_H
